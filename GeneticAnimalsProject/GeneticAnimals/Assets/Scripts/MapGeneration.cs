﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGeneration : Singleton<MapGeneration> {

    public SO_MapHeWhAnimals mapHeWhAnim;
    public SO_FloatVariable percRich;
    public SO_FloatVariable percPoor;
    public SO_FloatVariable percDesert;
    public SO_FloatVariable percGlacial;
    public SO_FloatVariable percJungle;
    public SO_FloatVariable percTundra;
    public SO_GameMapVal SO_gameMapVal;
    public GameObject tile;
    public GameObject animal;
    public GameObject animalList;
    public GameObject mainCamera;
    public List<GameObject> mapTilesList = new List<GameObject>();

    float nrmPercRich = 0;
    float nrmPercPoor = 0;
    float nrmPercDesert = 0;
    float nrmPercGlacial = 0;
    float nrmPercTundra = 0;
    float nrmPercJungle = 0;

    public void StartGame()
    {
        SetMapValues();
        GenerateMap();
        GenerateAnimals();
    }

    public Vector3 GetRandomTilePosition()
    {
        int randomValue = Random.Range(0, mapTilesList.Count);
        return mapTilesList[randomValue].transform.position;
    }

    void SetMapValues()
    {
        float sum = percRich.floatVariable + percPoor.floatVariable + percDesert.floatVariable + percGlacial.floatVariable + percTundra.floatVariable + percJungle.floatVariable;
        nrmPercRich = (percRich.floatVariable * 100) / sum; 
        nrmPercPoor = (percPoor.floatVariable * 100) / sum;
        nrmPercDesert = (percDesert.floatVariable * 100) / sum;
        nrmPercGlacial = (percGlacial.floatVariable * 100) / sum;
        nrmPercTundra = (percTundra.floatVariable * 100) / sum;
        nrmPercJungle = (percJungle.floatVariable * 100) / sum;

        //print("rich " + nrmPercRich);
        //print("poor " + nrmPercPoor);
        //print("desert " + nrmPercDesert);
        //print("glacial " + nrmPercGlacial);
        //print("jungle " + nrmPercJungle);
        //print("tundra " + nrmPercTundra);

        percRich.floatVariable = nrmPercRich;
        percPoor.floatVariable = nrmPercPoor;
        percDesert.floatVariable = nrmPercDesert;
        percGlacial.floatVariable = nrmPercGlacial;
        percTundra.floatVariable = nrmPercTundra;
        percJungle.floatVariable = nrmPercJungle;

    }

    void GenerateMap()
    {
        //generates the map from 0,0,0
        mapTilesList.Clear();
        bool offset = false;
        Vector3 generator = Vector3.zero;
        float distVert = SO_gameMapVal.distTileVert;
        float distHor = SO_gameMapVal.distTileHor;
        GameObject spawned;

        for(int i = 0; i < mapHeWhAnim.width; i++)
        {
            for(int k = 0; k < mapHeWhAnim.height; k++)
            {
                //move camera if in the middle of the map
                //if (i == Mathf.Floor(mapWidth / 2) && k == Mathf.Floor(mapHeight / 2))
                if (i == 5 && k == 5)
                    mainCamera.transform.position = generator;
                //instantiate
                spawned = Instantiate(tile, generator, Quaternion.identity, transform);
                ChooseClass(spawned);
                mapTilesList.Add(spawned);
                //Z offsett
                generator += new Vector3(0, 0, distVert);
            }
            //return to top + offset
            if(!offset)
                generator += new Vector3(distHor, 0, -((distVert * mapHeWhAnim.height) + (distVert / 2)));
            else
                generator += new Vector3(distHor, 0, -((distVert * mapHeWhAnim.height) - (distVert / 2)));

            offset = !offset;
        }
    }

    void GenerateAnimals()
    {
        int count = mapTilesList.Count;
        for (int i = 0; i < mapHeWhAnim.animals; i++)
        {
            bool chosen = false;
            //i chose a random tile untill i find one empty (no animal)
            while (!chosen)
            {
                int random = Random.Range(0, count);
                if (!(mapTilesList[random].GetComponent<TileStats>().occupied))
                {
                    //instantiate an animal child of the animalList object
                    Instantiate(animal, mapTilesList[random].transform.position + (Vector3.up * SO_gameMapVal.animalsHeight), Quaternion.identity, animalList.transform);
                    mapTilesList[random].GetComponent<TileStats>().occupied = true;
                    chosen = true;
                }
            }
        }
    }

    void ChooseClass(GameObject tile)
    {
        TileStats.TileClass randomClass = TileStats.TileClass.Rich;

        float randomChoice = Random.Range(0f, 100f);

        if (randomChoice < nrmPercRich)
            randomClass = TileStats.TileClass.Rich;
        else if (randomChoice < nrmPercRich + nrmPercPoor)
            randomClass = TileStats.TileClass.Poor;
        else if (randomChoice < nrmPercRich + nrmPercPoor + nrmPercDesert)
            randomClass = TileStats.TileClass.Desert;
        else if (randomChoice < nrmPercRich + nrmPercPoor + nrmPercDesert + nrmPercGlacial)
            randomClass = TileStats.TileClass.Glacial;
        else if (randomChoice < nrmPercRich + nrmPercPoor + nrmPercDesert + nrmPercGlacial + nrmPercTundra)
            randomClass = TileStats.TileClass.Tundra;
        else if (randomChoice >= nrmPercRich + nrmPercPoor + nrmPercDesert + nrmPercGlacial + nrmPercTundra)
            randomClass = TileStats.TileClass.Jungle;

        tile.GetComponent<TileStats>().tileClass = randomClass;
    }
}
