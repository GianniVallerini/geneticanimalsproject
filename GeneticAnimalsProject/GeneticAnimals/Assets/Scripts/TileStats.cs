﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileStats : MonoBehaviour {

    public enum TileClass
    {
        Rich, //food++
        Poor, //food--
        Desert, //food-- temp++
        Glacial, //food-- temp--
        Tundra, //food++ temp--
        Jungle  //food++ temp++

    };

    public SO_GameValues SO_gameValues;
    public float food;
    public float temperature;
    public float foodUnit;
    public float tempUnit;
    public bool occupied = false;

    public TileClass tileClass = TileClass.Rich;

    void Start()
    {
        foodUnit = (SO_gameValues.maxFood - SO_gameValues.minFood) / 100;
        tempUnit = (SO_gameValues.maxTemp - SO_gameValues.minTemp) / 50;
    }

    public void StatsChange()
    {
        ClassEffect(tileClass);
        CapStats();
    }

    void CapStats()
    {
        if (food > SO_gameValues.maxFood)
            food = SO_gameValues.maxFood;
        if (food < SO_gameValues.minFood)
            food = SO_gameValues.minFood;

        if (temperature > SO_gameValues.maxTemp)
            temperature = SO_gameValues.maxTemp;
        if (temperature < SO_gameValues.minTemp)
            temperature = SO_gameValues.minTemp;
    }

    void ClassEffect(TileClass tileClass)
    {
        float randomMult = Random.Range(1f, 2f);
        int random = Random.Range(0, 2);
        switch (tileClass)
        {
            case TileClass.Rich:
                food += foodUnit * (randomMult * 2.5f);
                if (random == 0)
                    temperature -= tempUnit * randomMult;
                else
                    temperature += tempUnit * randomMult;
                break;
            case TileClass.Poor:
                food += foodUnit * (randomMult * 0.5f);
                if (random == 0)
                    temperature -= tempUnit * randomMult;
                else
                    temperature += tempUnit * randomMult;
                break;
            case TileClass.Desert:
                food += foodUnit * (randomMult * 0.5f);
                if (random == 0)
                    temperature -= tempUnit * (randomMult * 0.5f);
                else
                    temperature += tempUnit * (randomMult * 2);
                break;
            case TileClass.Glacial:
                food += foodUnit * (randomMult * 0.5f);
                if (random == 0)
                    temperature -= tempUnit * (randomMult * 2);
                else
                    temperature += tempUnit * (randomMult * 0.5f);
                break;
            case TileClass.Tundra:
                food += foodUnit * (randomMult * 2.5f);
                if (random == 0)
                    temperature -= tempUnit * (randomMult * 2);
                else
                    temperature += tempUnit * (randomMult * 0.5f);
                break;
            case TileClass.Jungle:
                food += foodUnit * (randomMult * 2.5f);
                if (random == 0)
                    temperature -= tempUnit * (randomMult * 0.5f);
                else
                    temperature += tempUnit * (randomMult * 2);
                break;
        }
    }

}
