﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new_camera_stats", menuName = "New Camera Stats")]
public class CameraStats : ScriptableObject
{
    [Header("Movement Variables")]
    public float movementSpeed = 1f;
    [Header("Rotation Variables")]
    public float yawSpeed = 1f;
    [Header("Zoom Variables")]
    public float zoomSpeed = 1f;
    public Vector2 minMaxZoom = new Vector2(5f, 10f);
}
