﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{

    [Header("References")]
    public GameObject cameraObject;
    public GameObject managerObject;
    [SerializeField] private CameraStats stats = null;

    [Header("Input Keys")]
    [SerializeField] private KeyCode moveForward = default;
    [SerializeField] private KeyCode moveBackward = default;
    [SerializeField] private KeyCode moveRight = default;
    [SerializeField] private KeyCode moveLeft = default;
    [SerializeField] private KeyCode yawRight = default;
    [SerializeField] private KeyCode yawLeft = default;
    [SerializeField] private KeyCode zoomIn = default;
    [SerializeField] private KeyCode zoomOut = default;

    private void Update()
    {
        CheckInputs();
    }

    private void CheckInputs()
    {
        #region Movement

        var movementInput = Vector2.zero;
        if (Input.GetKey(moveForward))
        {
            movementInput.y++;
        }
        if (Input.GetKey(moveBackward))
        {
            movementInput.y--;
        }
        if (Input.GetKey(moveRight))
        {
            movementInput.x++;
        }
        if (Input.GetKey(moveLeft))
        {
            movementInput.x--;
        }

        Move(movementInput);

        #endregion

        #region Rotation

        var rotationInput = 0f;

        if (Input.GetKey(yawRight))
        {
            rotationInput++;
        }
        if (Input.GetKey(yawLeft))
        {
            rotationInput--;
        }

        Rotate(rotationInput);

        #endregion

        #region Zoom

        var zoomInput = 0f;

        if (Input.GetKey(zoomIn))
        {
            zoomInput++;
        }
        if (Input.GetKey(zoomOut))
        {
            zoomInput--;
        }

        Zoom(zoomInput);

        #endregion
    }

    private void Move(Vector2 movementInput)
    {
        movementInput *= stats.movementSpeed * Time.deltaTime;
        managerObject.transform.position += managerObject.transform.forward * movementInput.y;
        managerObject.transform.position += managerObject.transform.right * movementInput.x;
    }

    private void Rotate(float rotationInput)
    {
        rotationInput *= stats.yawSpeed * Time.deltaTime;
        managerObject.transform.Rotate(Vector3.up, rotationInput, Space.World);
    }

    private void Zoom(float zoomInput)
    {
        zoomInput *= stats.zoomSpeed * Time.deltaTime;
        cameraObject.transform.localPosition =
            new Vector3(0, Mathf.Clamp(cameraObject.transform.localPosition.y + zoomInput, stats.minMaxZoom.x, stats.minMaxZoom.y), 0);
    }
}
