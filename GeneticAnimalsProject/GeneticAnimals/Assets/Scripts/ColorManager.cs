﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour {

    public SO_GameColors SO_GCSet;
    public SO_GameValues SO_GV;
    public GameObject temperatureObj;
    public GameObject foodObj;
    public TileStats tileStats;

    float maxFood;
    float minFood;
    float maxTemp;
    float minTemp;
    float normFood;
    float normTemp;
    Color foodMaxClr;
    Color foodMinClr;
    Color tempMaxClr;
    Color tempMinClr;
    Color foodColor;
    Color tempColor;

    void OnEnable()
    {
        maxFood = SO_GV.maxFood;
        minFood = SO_GV.minFood;
        maxTemp = SO_GV.maxTemp;
        minTemp = SO_GV.minTemp;
        foodMaxClr = SO_GCSet.foodMax;
        foodMinClr = SO_GCSet.foodMin;
        tempMaxClr = SO_GCSet.temperatureMax;
        tempMinClr = SO_GCSet.temperatureMin;
    }

    public void OnTick()
    {
        //normalize food and temperature value
        normFood = Normalize(tileStats.food, minFood, maxFood);
        normTemp = Normalize(tileStats.temperature, minTemp, maxTemp);

        //create new colors
        foodColor = CreateNormalizedColor(foodMinClr, foodMaxClr, normFood);
        tempColor = CreateNormalizedColor(tempMinClr, tempMaxClr, normTemp);

        //assign new colors
        foodObj.GetComponent<MeshRenderer>().material.color = foodColor;
        temperatureObj.GetComponent<MeshRenderer>().material.color = tempColor;
    }

    float Normalize(float value, float min, float max)
    {
        float result;
        result = (value - min) / (max - min);
        return result;
    }

    Color CreateNormalizedColor(Color minColor, Color maxColor, float normValue)
    {
        float RValue = Mathf.Lerp(minColor.r, maxColor.r, normValue);
        float GValue = Mathf.Lerp(minColor.g, maxColor.g, normValue);
        float BValue = Mathf.Lerp(minColor.b, maxColor.b, normValue);
        return new Color(RValue, GValue, BValue, 1);
    }
}