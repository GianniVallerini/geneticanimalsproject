﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BiomeChanger : MonoBehaviour
{

    public SO_TileClassVariable tileClass;
    public SO_FoodOrTemp valueToChange;
    public SO_FloatVariable value;
    public SO_BoolVariable isBiomeChanger;

    List<TileStats> selectedTiles = new List<TileStats>();
    int tilesLayer = 1 << 8;

    void Update()
    {
        TargetTile();
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (isBiomeChanger.boolVariable)
                ChangeTilesClass();
            else
                ChangeValues();
        }
    }

    public void TargetTile()
    {

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 300f, tilesLayer))
        {
            transform.position = hit.transform.position;
        }
    }

    void ChangeTilesClass()
    {
        foreach(TileStats ts in selectedTiles)
        {
            ts.tileClass = tileClass.classVariable;
        }
    }

    void ChangeValues()
    {
        foreach (TileStats ts in selectedTiles)
        {
            if (valueToChange.variable == SO_FoodOrTemp.FoodOrTemp.FOOD)
                ts.food = value.floatVariable;
            else
                ts.temperature = value.floatVariable;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Tile")
        {
            selectedTiles.Add(other.GetComponent<TileStats>());
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Tile")
        {
            selectedTiles.Remove(other.GetComponent<TileStats>());
        }
    }

}
