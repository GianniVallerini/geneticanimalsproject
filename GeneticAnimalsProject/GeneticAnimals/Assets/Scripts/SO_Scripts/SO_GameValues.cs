﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SO_GameValues", menuName = "GameValues")]
public class SO_GameValues : ScriptableObject {

    public float maxFood = 100f;
    public float minFood = 0f; 
    public float maxTemp = 40f;
    public float minTemp = 0f;
    public float tickSpeed = 1f;
}
