﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SO_IntVariable", menuName = "IntVariable")]
public class SO_IntVariable : ScriptableObject
{

    public int intVariable = 0;

}
