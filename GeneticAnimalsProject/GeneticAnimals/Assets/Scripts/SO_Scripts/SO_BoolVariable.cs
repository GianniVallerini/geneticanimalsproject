﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SO_BoolVariable", menuName = "BoolVariable")]
public class SO_BoolVariable : ScriptableObject
{

    public bool boolVariable = false;

}
