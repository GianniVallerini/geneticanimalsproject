﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SO_BrainWeights", menuName = "BrainWeights")]
public class SO_BrainWeights : ScriptableObject {

    public List<float> weightList;

    public void CopyWeights(SO_BrainWeights from, SO_BrainWeights to)
    {
        to.weightList.Clear();
        for(int i = 0; i < from.weightList.Count; i++)
        {
            to.weightList.Add(5.26f);
            to.weightList[i] = from.weightList[i];           
        }
    }

}
