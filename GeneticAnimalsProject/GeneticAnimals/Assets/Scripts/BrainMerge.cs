﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainMerge : MonoBehaviour {

	public SO_BrainWeights outputBrain;
    public string newBrainName = "Brain_";
    public int brainCounter = 000;
    public GameEvent showBrainHUD;

    [HideInInspector]
    public string jsonLastBrain;

    List<SO_BrainWeights> inputBrains = new List<SO_BrainWeights>();
    List<Animal> animalsList = new List<Animal>();

    public void MergeBrains()
    {
        inputBrains.Clear();
        Animal[] animals = FindObjectsOfType<Animal>(); 
        foreach(Animal anml in animals)
        {
            inputBrains.Add(anml.learningBrain);
        }

        for(int i = 0; i < outputBrain.weightList.Count; i++)
        {
            outputBrain.weightList[i] = 0;
            for(int k = 0; k < inputBrains.Count; k++)
            {
                outputBrain.weightList[i] += inputBrains[k].weightList[i];
            }
        }

        NormalizeWeights(outputBrain.weightList);

        brainCounter++;
        jsonLastBrain = JsonUtility.ToJson(outputBrain, true);
        showBrainHUD.Raise();

        StartCoroutine(CopyBrains());
    }

    IEnumerator CopyBrains()
    {
        yield return new WaitForSeconds(0.1f);
        Animal[] animalsArray = FindObjectsOfType<Animal>();
        animalsList.Clear();

        foreach (Animal anim in animalsArray)
        {
            animalsList.Add(anim);
        }

        foreach (Animal anim in animalsList)
        {
            anim.learningBrain.CopyWeights(outputBrain, anim.learningBrain);
        }
    }

    void NormalizeWeights(List<float> weights)
    {
        float sum = 0;
        foreach (float ft in weights)
            sum += ft;

        for (int i = 0; i < weights.Count; i++)
            weights[i] = (100 * weights[i]) / sum;
    }
}
