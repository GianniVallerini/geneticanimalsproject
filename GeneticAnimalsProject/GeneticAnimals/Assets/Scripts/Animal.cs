﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Animal : MonoBehaviour
{
    public struct IndexPoint
    {
        public int index;
        public float point;
    }

    public bool lockOnDeath = false;
    public GameObject animal;
    public GameEvent brainMergeEvent;
    public GameEvent clockToManualEvent;
    public GameObject deathParticle;
    public GameObject bodyMesh;
    public SO_GameValues SO_gameValues;
    public SO_GameMapVal SO_gameMapVal;
    public SO_BrainWeights startingBrain;
    public List<Vector3> EmitterPositions = new List<Vector3>();
    public Color maxHpColor;
    public Color normalHpColor;
    public Color minHpColor;
    private float health = 100;
    public float Health
    {
        get
        {
            return health;
        }
        set
        {
            if(health != value)
            {
                health = value;
                if (health > 100)
                {
                    health = 100;
                }

                UpdateBodyColor();

                if (health < 0)
                {
                    Die();
                }
            }
        }
    }
    public float bonusMalus = 0;
    public float eatingFactor = 5;

    [HideInInspector]
    public SO_BrainWeights learningBrain;
    public List<float> innerBrain = new List<float>();

    int landingIndex;
    GameObject landingTile;
    GameObject animals;
    GameObject newAnimal;
    int tilesLayer = 1 << 8;
    List<IndexPoint> EmitterToPoint = new List<IndexPoint>();
    RaycasterGenerator raycasterGen;

    void Start()
    {
        learningBrain = ScriptableObject.CreateInstance<SO_BrainWeights>();
        learningBrain.weightList = new List<float>();
        learningBrain.CopyWeights(startingBrain, learningBrain);
        innerBrain = learningBrain.weightList;
        raycasterGen = GetComponentInChildren<RaycasterGenerator>();
        animals = GameObject.FindGameObjectWithTag("Animals");
        newAnimal = animal;
    }

    public void OnTickActions ()
    {
        raycasterGen.FindTiles();
        CreatePointsList();
        MoveToTile(ChooseTile(EmitterToPoint));
    }

    public void OnPostTickActions ()
    {
        if (landingTile != null)
        {
            CalculateHealth(landingTile.GetComponent<TileStats>(), landingIndex);
            Eat();
        }
    }

    public void Eat()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 100f, tilesLayer))
        {
            TileStats tileStats = hit.transform.gameObject.GetComponent<TileStats>();
            tileStats.food -= eatingFactor;
        }
    }

    public void CreatePointsList ()
    {
        EmitterToPoint.Clear();
        IndexPoint current = new IndexPoint();
        int index = 0;
        float sum = 0;
        foreach (Vector3 v3 in EmitterPositions)
        {
            RaycastHit hit;
            if (Physics.Raycast(v3, Vector3.down, out hit, 1000f, tilesLayer))
            {
                //print(hit.transform.gameObject.name);
                TileStats tileStats = hit.transform.gameObject.GetComponent<TileStats>();
                current.index = index;
                current.point = CalcBonusMalus(tileStats.food, tileStats.temperature);
                EmitterToPoint.Add(current);
                sum += current.point;
            }
            index++;
        }
        float midPoints = sum / EmitterToPoint.Count;
        for (int i = EmitterToPoint.Count; i < EmitterPositions.Count; i++)
        {
            current.index = 666;
            current.point = midPoints;
            EmitterToPoint.Add(current);
        }
        SortByPoints(EmitterToPoint);
    }

    public float CalcBonusMalus (float food, float temp)
    {
        float point = 0;
        float normalizedFood = Normalize(food, SO_gameValues.minFood, SO_gameValues.maxFood);
        float normalizedTemp = Normalize(temp, SO_gameValues.minTemp, SO_gameValues.maxTemp);

        //modificatore cibo
        if (normalizedFood < 0.15f) point = -20;
        else if (normalizedFood < 0.2f) point = -8;
        else if (normalizedFood < 0.4f) point = 0;
        else if (normalizedFood < 0.6f) point = 5;
        else if (normalizedFood < 0.8f) point = 10;
        else point = 15;
        //modificatore temperatura
        if (normalizedTemp < 0.1f) point += -15;
        else if (normalizedTemp < 0.2f) point += -5;
        else if (normalizedTemp < 0.3f) point += 0;
        else if (normalizedTemp < 0.4f) point += 8;
        else if (normalizedTemp < 0.6f) point += 15;
        else if (normalizedTemp < 0.7f) point += 8;
        else if (normalizedTemp < 0.8f) point += 0;
        else if (normalizedTemp < 0.9f) point += -5;
        else point += -15;

        return point;
    }

    void SortByPoints (List<IndexPoint> list)
    {
        list.Sort((f1, f2) => f1.point.CompareTo(f2.point));
    }

    Vector3 ChooseTile(List<IndexPoint> list)
    {
        int randomChoice = 0;
        randomChoice = WeightRandom(learningBrain, list);

        if (list.Count > randomChoice &&
            EmitterPositions.Count > list[randomChoice].index)
        {

            RaycastHit hit;
            Physics.Raycast(EmitterPositions[list[randomChoice].index], Vector3.down, out hit, 1000f, tilesLayer);

            landingIndex = randomChoice;
            landingTile = hit.transform.gameObject;

            return new Vector3(hit.transform.position.x, hit.transform.position.y + SO_gameMapVal.animalsHeight, hit.transform.position.z);
        }
        else
        {
            return transform.position;
        }
    }

    int WeightRandom(SO_BrainWeights brain, List<IndexPoint> indexPointList)
    {
        bool found = false;
        List<float> list = brain.weightList;
        float randomChoice = Random.Range(0f, 100f);
        int i = list.Count - 1;
        float t = 100;

        while (!found)
        {
            t -= list[i];
            if (randomChoice >= t && indexPointList[i].index != 666)
            {
                found = true;
                return i;
            }
            if(i == 0)
            {
                found = true;
                return i;
            }
            i--;
        }
        return 0;
    }

    void MoveToTile(Vector3 position)
    {
        //disoccupy the current tile
        RaycastHit hit;
        if(Physics.Raycast(transform.position, Vector3.down, out hit, 100f, tilesLayer))
        {
            hit.transform.gameObject.GetComponent<TileStats>().occupied = false;
        }

        if (transform.position != position)
        {
            var newDirection = new Vector3(position.x, transform.position.y, position.z) - transform.position;
            if (newDirection.magnitude > 0)
            {
                transform.forward = new Vector3(position.x, transform.position.y, position.z) - transform.position;
            }
            StartCoroutine(MoveAnimation(1 / SO_gameValues.tickSpeed, transform.position, position, Random.Range(1f, 1.5f)));
        }
    }

    void CalculateHealth(TileStats tile, int pointIndex)
    {
        List<float> weights = learningBrain.weightList;
        int count = weights.Count;
        float bonMal = CalcBonusMalus(tile.food, tile.temperature);
        float normalizedBonMal = Normalize(bonMal, -35, 30);

        Health += bonMal;

        weights[pointIndex] += normalizedBonMal;
        if (weights[pointIndex] < 0)
        {
            weights[pointIndex] = 0;
        }

        for (int i = 0; i < count; i++)
        {
            if(i != pointIndex)
            {
                weights[i] -= (normalizedBonMal) / 18;
                if (weights[i] < 0)
                    weights[i] = 0.1f;
            }
        }
        NormalizeWeights(weights);
    }

    void Die()
    {
        brainMergeEvent.Raise();
        Instantiate(deathParticle, transform.position, Quaternion.LookRotation(Vector3.up));
        StartCoroutine(DeathCor());
    }

    void CreateChild()
    {

        var spawned = Instantiate(newAnimal, MapGeneration.Instance.GetRandomTilePosition() + Vector3.up * 5, transform.rotation, animals.transform);
        spawned.GetComponent<Animal>().Health = 100;
        spawned.GetComponent<Animal>().MoveDown();
    }

    private void UpdateBodyColor()
    {
        var bodyRend = bodyMesh.GetComponent<MeshRenderer>();
        var colorToSet = Health < 50 ? Color.Lerp(minHpColor, normalHpColor, Mathf.InverseLerp(0, 50, Health)) :
                                       Color.Lerp(normalHpColor, maxHpColor, Mathf.InverseLerp(50, 100, Health));
        bodyRend.material.color = colorToSet;
    }

    public void MoveDown()
    {
        StartCoroutine(MoveDownCor());
    }

    IEnumerator DeathCor()
    {
        yield return new WaitForSeconds(0.09f);
        CreateChild();
        if (lockOnDeath)
        {
            clockToManualEvent.Raise();
        }
        Destroy(gameObject);
    }

    IEnumerator MoveDownCor()
    {
        float t = 0;
        float maxT = 0.04f;
        float lerp = 0;
        RaycastHit hit;
        Vector3 startingPos = transform.position;
        Vector3 tilePos = Vector3.zero;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 1000f, tilesLayer))
        {
            tilePos = hit.transform.position;
        }

        while (t < maxT)
        {
            t += Time.deltaTime;
            lerp = t / maxT;
            transform.position = new Vector3(tilePos.x, Mathf.Lerp(startingPos.y, tilePos.y + SO_gameMapVal.animalsHeight, lerp), tilePos.z);
            yield return null;
        }
    }

    void NormalizeWeights(List<float> weights)
    {
        float sum = 0;
        foreach (float ft in weights)
        {
            sum += ft;
        }

        for (int i = 0; i < weights.Count; i++)
        {
            weights[i] = 100 * weights[i] / sum;
        }
    }

    IEnumerator MoveAnimation(float tickTimer, Vector3 startingPos, Vector3 endingPos, float jumpHeight)
    {
        float t = 0;
        float lerp = 0;
        float jumpLerp = 0;
        while(t < tickTimer)
        {
            t += Time.deltaTime;
            lerp = t / tickTimer;
            jumpLerp = Mathf.Sin(lerp*3.14f);
            bodyMesh.transform.position = new Vector3(bodyMesh.transform.position.x, Mathf.Lerp(startingPos.y, startingPos.y + jumpHeight, jumpLerp), bodyMesh.transform.position.z);
            transform.position = new Vector3(Mathf.Lerp(startingPos.x, endingPos.x, lerp), startingPos.y, Mathf.Lerp(startingPos.z, endingPos.z, lerp));
            yield return null;
        }
        //occupy the current tile
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 100f, tilesLayer))
        {
            hit.transform.gameObject.GetComponent<TileStats>().occupied = true;
        }
        transform.position = endingPos;
    }

    float Normalize(float value, float min, float max)
    {
        float result;
        result = (value - min) / (max - min);
        return result;
    }
}
