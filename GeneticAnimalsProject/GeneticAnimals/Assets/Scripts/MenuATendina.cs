﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuATendina : MonoBehaviour
{

    public GameEvent menuOpenEv;
    public GameEvent menuCloseEv;
    public float openCloseSpeed;
    public bool open = false;
    public bool opening = false;

    public void OpenClose()
    {
        if (!opening)
        {
            if (open)
                Disappear();
            else
                Appear();

            open = !open;
        }
    }

    void Disappear()
    {
        StartCoroutine(DisappearCor());
        menuCloseEv.Raise();
    }

    void Appear()
    {
        StartCoroutine(AppearCor());
        menuOpenEv.Raise();
    }
	
    IEnumerator DisappearCor()
    {
        opening = true;
        float t = 0;
        float maxT = openCloseSpeed;
        float lerp = 0;
        float startX = transform.localPosition.x;
        while (t < maxT)
        {
            t += Time.deltaTime;
            lerp = t / maxT;
            transform.localPosition = new Vector3(startX + Mathf.Lerp(0, 350, lerp), transform.localPosition.y, transform.localPosition.z);
            yield return null;
        }
        opening = false;
    }

    IEnumerator AppearCor()
    {
        opening = true;
        float t = 0;
        float maxT = openCloseSpeed;
        float lerp = 0;
        float startX = transform.localPosition.x;
        while (t < maxT)
        {
            t += Time.deltaTime;
            lerp = t / maxT;
            transform.localPosition = new Vector3(startX - Mathf.Lerp(0, 350, lerp), transform.localPosition.y, transform.localPosition.z);
            yield return null;
        }
        opening = false;
    }

}
