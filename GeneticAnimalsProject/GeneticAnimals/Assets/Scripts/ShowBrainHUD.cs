﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowBrainHUD : MonoBehaviour
{

    public BrainMerge brainMergeScript;

    public void UpdateText()
    {
        GetComponent<Text>().text = brainMergeScript.newBrainName + brainMergeScript.brainCounter + ":\n" + brainMergeScript.jsonLastBrain;
    }

}
