﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleClock : MonoBehaviour {

    public KeyCode toggleInput;
    public GameEvent toAutomaticEvent;
    public GameEvent toManualEvent;

    bool automatic = false;

    void Update()
    {
        if(Input.GetKeyDown(toggleInput))
        {
            if (automatic)
                toManualEvent.Raise();
            else
                toAutomaticEvent.Raise();
        }
    }

    public void SwitchAutomatic()
    {
        automatic = !automatic;
    }
}
